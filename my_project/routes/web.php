<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index','PageController@index' );

Route::get('/about','PageController@about' );

Route::get('/photo', 'PageController@photo');

Route::get('/services','ServicesController@index' );

Route::get('/services/{package_type}','ServicesController@show');

Route::get('/contact','PageController@contact' );



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){
	Route::get('/admin/index', 'Admin\AdminController@index');
    Route::get('/admin/index/create', 'Admin\AdminController@create');
    Route::post('/admin/index', 'Admin\AdminController@store');
    Route::get('/admin/index/{id}', 'Admin\AdminController@edit');
    Route::put('/admin/index/{id}', 'Admin\AdminController@update');
    Route::delete('/admin/index/{id}', 'Admin\AdminController@destroy');
});