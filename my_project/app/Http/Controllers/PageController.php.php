<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
	 * home page
	 * @return view [description]
	 */
    public function index(){
    	$subtitle = 'Welcome';
    	return view('main.index',compact('subtitle'));
    }

    /**
	 * about page
	 * @return view [description]
	 */
    public function about(){
    	$subtitle = 'About Us';
    	return view('main.about',compact('subtitle'));
    }

    /**
	 * photo page
	 * @return view [description]
	 */
    public function photo(){
    	$subtitle = 'Photography';
    	return view('main.photo',compact('subtitle'));
    }


    /**
	 * contact page
	 * @return view [description]
	 */
    public function contact(){
    	$subtitle = 'Contact us:';
    	return view('main.contact',compact('subtitle'));
    }
}
