<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Str;
use App\Services;
use \Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::latest()->get();
        return view('admin.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = request()->validate([
            'package_type'=>'required|regex:/(^[a-zA-Z\s?\-?]{3,100})+$/',
            'session_time'=>'required|regex:/(^[0-9a-zA-Z\s?\-?]{3,100})+$/',
            'photo_quality'=>'required|regex:/^[0-9]{1,}\s?[A-Za-z]{1,}$/',
            'photo_size'=>'required|regex:/^[0-9]{1,3}[\*][0-9]{1,3}$/',
            'no_of_photos'=>'required|regex:/^[0-9]{1,4}$/',
            'delivery_method'=>'required|regex://',
            'price'=>'required|regex:/^[0-9]{1,4}$/',
            'image'=>'required|regex:/(^[a-zA-Z\s?\-?\.]{3,100})+$/',
            'photographer_name'=>'required|regex:/(^[a-zA-Z\s?\-?]{3,100})+$/',
            'description'=>'required|string',
            'availability'=>'required|regex:/^[a-zA-Z]+$/'
        ]);
        Services::create($valid);
        return redirect('/admin/index');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Services::find($id);
        return view('admin.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = request()->validate([
            'package_type'=>'required',
            'session_time'=>'required',
            'photo_quality'=>'required',
            'photo_size'=>'required',
            'no_of_photos'=>'required',
            'delivery_method'=>'required',
            'price'=>'required',
            'image'=>'required',
            'photographer_name'=>'required',
            'description'=>'required',
            'availability'=>'required'
        ]);
        $service = Services::find($id);
        $service->package_type = request('package_type');
        $service->session_time = request('session_time');
        $service->photo_quality = request('photo_quality');
        $service->photo_size = request('photo_size');
        $service->no_of_photos = request('no_of_photos');
        $service->delivery_method = request('delivery_method');
        $service->price = request('price');
        $service->image = request('image');
        $service->photographer_name = request('photographer_name');
        $service->description = request('description');
        $service->availability = request('availability');
        $service->created_at = Carbon::now();
        $service->updated_at = Carbon::now();
        $service->save();
        return redirect('/admin/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Services::find($id);
        $service->delete();
        return redirect('/admin/index');
    }
}
