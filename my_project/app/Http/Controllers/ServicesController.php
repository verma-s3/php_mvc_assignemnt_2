<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services;

class ServicesController extends Controller
{
    
    /**
	 * services page
	 * @return view [description]
	 */
    public function index(){
    	$subtitle = 'Services';
    	$services = Services::tableshow();
    	return view('main.services',compact('subtitle','services'));
    }
    /**
	 * servicesDetail page to show detail of service
	 * @return view [description]
	 */
    public function show($package_type){
    	$subtitle = 'Services';
    	$service = Services::where('package_type',$package_type)->firstOrFail();
    	return view('main.services_detail',compact('subtitle','service'));
    }
}

