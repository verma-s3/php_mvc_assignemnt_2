<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
	protected $fillable = ['package_type','session_time','photo_quality','photo_size','no_of_photos','delivery_method','price','image','photographer_name','description','availability'];
    /**
	 * Table show -- show the details of table
	 * @return records
	 */
    public static function tableShow(){
    	return self::latest()->get();
    }
}
