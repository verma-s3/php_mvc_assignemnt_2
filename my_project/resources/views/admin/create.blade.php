@extends('admin.admin-layout')

@section('content')
@auth
<!-- Page Content Column -->
<div class="col-lg-12">

    <h1>Create a new Service</h1>
    <p><a class="btn btn-primary" href="/admin/index">Back</a></p>

    @include('partials.error')

    <form action="/admin/index" method="post">

        @csrf

        <div class="form-group">
            <label for="package_type">Package type</label><br />
            <input id="package_type" type="text" name="package_type" class="form-control" value="{{ old('package_type') }}" />

            <label for="session_time">Session Time</label><br />
            <input id="session_time" type="text" name="session_time" class="form-control" value="{{ old('session_time') }}" />

            <label for="photo_quality">Photo Quality</label><br />
            <input id="photo_quality" type="text" name="photo_quality" class="form-control" value="{{ old('photo_quality') }}" />

            <label for="photo_size">Photo Size</label><br />
            <input id="photo_size" type="text" name="photo_size" class="form-control" value="{{ old('photo_size') }}" />

            <label for="no_of_photos">Photo Quantity</label><br />
            <input id="no_of_photos" type="text" name="no_of_photos" class="form-control" value="{{ old('no_of_photos') }}" />

            <label for="delivery_method">Delivery Method</label><br />
            <input id="delivery_method" type="text" name="delivery_method" class="form-control" value="{{ old('delivery_method') }}" />

            <label for="price">Price</label><br />
            <input id="price" type="text" name="price" class="form-control" value="{{ old('price') }}" />

            <label for="image">Image</label><br />
            <input id="image" type="text" name="image" class="form-control" value="{{ old('image') }}" />

            <label for="photographer_name">Photographer Name</label><br />
            <input id="photographer_name" type="text" name="photographer_name" class="form-control" value="{{ old('photographer_name') }}" />

            <label for="description">Description</label><br />
            <input id="description" type="text" name="description" class="form-control" value="{{ old('description') }}" />

            <label for="availability">Availability</label><br />
            <input id="availability" type="text" name="availability" class="form-control" value="{{ old('availability') }}" />
        </div>
        <!-- <div class="form-group">
            <label for="body">Body</label><br />
            <textarea name="body" id="body" class="form-control">{{ old('body') }}</textarea>
        </div> -->
        <div class="form-group">
            <button class="btn btn-primary">Submit</button>
        </div>

    </form>

</div>
<!-- end page content -->
@endauth
@endsection