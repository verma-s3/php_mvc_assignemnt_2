@extends('admin.admin-layout')

@section('content')

@auth
<!-- Post Content Column -->
      <div class="col-lg-12">

        <h1>Services List</h1>
        <p><a class="btn btn-primary" href="/admin/index/create">Create a Service</a></p>

        <table class="table table-striped">
            <tr>
                <th>Services</th>
                <th>Session Time</th>
                <th>Quality</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Delivered by</th>
                <th>Action</th>
            </tr>

            @foreach($services as $service)
            <tr>
                <td>{{$service->package_type}}</td>
                <td>{{$service->session_time }}</td>
                <td>{{$service->photo_quality }}</td>
                <td>{{$service->photo_size }}</td>
                <td>{{$service->no_of_photos }}</td>
                <td>{{$service->price }}</td>
                <td>{{$service->delivery_method }}</td>
                <td><a class="btn btn-primary" href="/admin/index/{{ $service->id }}">edit</a>&nbsp;
                    <form class="form d-inline form-inline" 
                    action="/admin/index/{{ $service->id }}" 
                    method="post">
                        @csrf 
                        @method('DELETE')
                        <button class="btn btn-danger">delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>

      </div>
    @endauth
@endsection