<!DOCTYPE HTML>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <title>{{ config('app.name', 'Khera Color Lab and Digital Studio')}}</title> <!-- comapany name in title for good accessibility -->
    <meta charset="utf-8" />
<!--
    <meta property="og:url" content="http://salsa.uwdce.ca/~s.verma/HTML_CSS/project" />
    <meta property="og:title" content="Khera Color Lab and Digital Studio" />
    <meta property="og:description" content="Khera Studio is the one of the best photography studio in moga." />
    <meta property="og:site_name" content="Khera Color Lab and Digital Studio" />
    <meta property="og:image" itemprop="image primaryImageOfPage" content="http://salsa.uwdce.ca/~s.verma/HTML_CSS/project/Images/logo.png" />
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="80" />
    <meta name="description" content="Khera Color Lab and Digital Studio is one of the best studio in moga
    for the photography and videography services" />
    
    <meta name="keywords" content="Khera color lab and digital studio, Khera studio, 
    Khera lab,photographt, studio, photos, videos, pre-wedding, post-wedding, baby shoot" />
    
    It make my file to heavy than the 150 kb...
-->

    <!-- For Responsive Design -->
    <meta name="viewport" content="width=device-width" />    
    <link rel="icon" type="/images/jpg" href="favicon.jpg" />
    <link rel="apple-touch-icon" sizes="57x57" href="/Images/apple-icon-57x57.jpg" />
    <link rel="apple-touch-icon" sizes="72x72" href="/Images/apple-icon-72x72.jpg" />
    <link rel="apple-touch-icon" sizes="114x114" href="/Images/apple-icon-114x114.jpg" />
    <link rel="apple-touch-icon" sizes="144x144" href="/Images/apple-icon-144x144.jpg" /> 
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700%7cCookie:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700%7cRoboto+Slab:400,700" rel="stylesheet">
    <!-- external stylesheet -->
    <link rel="stylesheet" type="text/css" href="/styles/style.css" media="screen and (min-width:767px)" />
    <link rel="stylesheet" type="text/css" href="/styles/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="/styles/media.css" media="all and (max-width:960px)" />
    <link rel="stylesheet" type="text/css" href="/styles/mobile-media.css" media="all and (max-width:767px)" />
    
    
    <!--[if LTE IE 9]>
      <link rel="stylesheet" type="text/css" href="styles/ie.css" />
      <p style="color:red; font-size: 3em;">
        You need upgradation!!!!! Please update your brower version. 
      </p>
      <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('article');
        document.createElement('section');
        document.createElement('main');
        document.createElement('aside');
      </script>
    <![endif]-->
    @yield('style')
  
  </head>
  
  <body>
   
    <header><!-- header starting -->
      <div id="inner_header"><!-- inner_div starting -->
        <img src="/Images/logo.png" alt="logo" class="logo"  />
        <div id="phone"><!-- div for phone starting -->
          <img src="/Images/p.png" alt="phone_icon" style="height: 20px; width: 20px;" />
          <span>+91 97812-40006</span>
        </div><!-- div for phone ending -->
        @guest
          <a href="{{ route('login') }}" class="button">Login</a>
          @if(Route::has('register'))
            <a href="{{ route('register') }}" class="register" >Register</a>
          @endif
          @else
          <a href="{{route('logout')}}" class="button" onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">Logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        @endguest
        <div id="social_media"><!-- div for button starting -->
          <a href="https://www.facebook.com/" title="facebook link"><img src="/Images/facebook.png" alt="social media icons" style="margin-right: 25px;" class="facebook"/></a>
          <a href="https://www.instagram.com/" title="instagram link"><img src="/Images/instagram.png" alt="social media icons" style="margin-right: 25px;" class="instagram" /></a>
          <a href="https://twitter.com/" title="twitter link"><img src="/Images/twitter.png" alt="social media icons" class="twitter" /></a>
        </div> <!-- div for button ending -->
        <!-- navigation inside the nav  -->
        <nav id="main">
          <a id="toggle_menu" href="#navlist"><img src="/Images/menu-white.png" alt="menu" /></a>
          <ul id="navlist">
            <li><a href="/index" title="Home Page" >Home</a></li>
            <li><a href="/about" title="About us Page">About us</a></li>
            <li><a href="/photo" title="Photo Page">Photo</a></li>
            <li><a href="/services" title="Services Page">Services</a></li>
            <li><a href="/contact" title="Contact us Page">Contact us</a></li>
          </ul>
        </nav><!-- nav ending -->
      </div><!-- inner_div ending -->
    </header><!-- header ending -->
    
    @yield('content')
    
    <footer>
      <div class="footer_row"><!-- footer_row class starting -->
        <div id="about_us"><!-- about div starting -->
          <h1>About us</h1>
          <p>Khera Color Lab and Digital Studio is one of the best studio in moga. All types of services are available in our
             photography studio. Pre-wedding and Post-wedding shoots are available in trend those days........  </p>
          <a href="/about" style="color:#c00;text-decoration: none;">[Read more....]</a>
        </div><!-- about div ending -->
        <div id="services"><!-- services div starting -->
          <h1>Services</h1>
          <ul>
            <li>Wedding Functions</li>
            <li>Pre-wedding</li>
            <li>Post-wedding</li>
            <li>Special Events</li>
            <li>Kids Shoots</li>
            <li>Apple Album</li>
            <li>Portfolio</li>
            <li>Wedding box and cards</li>
            <li>Personalised Gifts</li>
          </ul>
        </div><!-- about div ending-->
        <div id="quick_links"><!-- quick links starting -->
          <h1>Quick links</h1>
          <ul>
            <li><a href="/about" title="about us page">About Us</a></li>
            <li><a href="/photo" title="photo page">Photo</a></li>
            <li><a href="/services" title="services page">Services</a></li>
            <li><a href="/contact" title="contact page">Contact Us</a></li>
          </ul>
        </div><!-- quick links div ending -->
        <div id="contact_us"><!-- contact us div starting -->
          <h1>Contact Us</h1>
          <h2>Timings:</h2>
          <p>10:00 AM- 08:00 PM</p>
          <h2>Call on:</h2>
          <p>97812-40006</p>
          <p>97816-04346</p>
          <h2>E-mail:</h2>
          <p>kheracolorlabmoga@gmail.com</p>
        </div><!-- contact us div ending -->
      </div><!-- class footer_row ending -->
      <p>&copy;2019. All rights are reserved.</p>
      
    </footer>
    
  </body>
</html>
