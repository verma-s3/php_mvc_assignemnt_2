@extends('main.layout')

@section('style')

    <style>
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */
    
     
      #inner h1{
        text-align: center;
      }
      
      #about_content{
        color: #fff;
        background: #ffc;
        display: block;
        max-width: 900px;
        margin: 50px auto 50px auto;
      }
      
      #about_content img{
        float: left;
        margin-right: 10px;
        border-right: 4px solid #f33;
      }
      
      #about_content p{
        padding-top: 10px;
        color: #000;
        margin-top: 0;
        padding-right: 10px;
        font-family: 'Roboto Slab','Times New Roman',serif;
        font-weight: 700;
      }
      
      #owner_details img{
        display: block;
        width: 50%;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
        border-radius: 50%;
      }
      
      .img-box {
        position: relative;
      }
        
      .img-box img {
        display: block;
        width: 100%;
        height: auto;
      }
      /* starting of hover effets */
      .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 50%;
        margin-left: auto;
        margin-right: auto;
        opacity: 0;
        border-radius: 50%;
        transition: .5s ease-in-out all;
        background-color: rgba(255,255,255,0.3);
      }
        
      .img-box:hover > .overlay {
        opacity: 1;
        cursor: pointer;  
        
      }
        
      .text {
        color: #fff;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%,-50%);
        -moz-transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        -o-transform: translate(-50%,-50%);
        transform: translate(-50%, -50%);
        text-align: center;
        font-family: 'Roboto Slab','Times New Roman',serif;
        font-weight: 400;
      }
      
    </style>

@endsection

@section('content')

    <section>
      <div id="container"><!-- container div id starting -->
        <div id="inner"><!-- inner div starting -->
          <h1><span class="underline">{{$subtitle}}</span></h1>
          <div id="owner_details"><!-- owner_details div starting -->
            <div class="img-box"><!-- img-box class starting -->
              <img src="Images/dp1.jpg" alt="photo" />
              <div class="overlay"><!-- overlay class starting  -->
                <div class="text">Khera Studio</div>
              </div><!-- overlay class ending -->
            </div><!-- img-box class ending -->
            <p style="margin-left: 40px; margin-right: 40px;">
              Mani is  the founder of Khera Lab And Digital Studio. He is very passoinate for his work. He is 
              man of few words. His work speaks for himself rather than his thoughts. He started his career at
              the age of 21. He is well known photograher in the punjab. what is unique about him is his photography
              skills. His passion for photography has always made him strong to work in hectic and hard schedule. 
            </p>
          </div><!-- owner_details div ending -->
          <hr />  
          <div id="about_content"><!-- about_content div starting -->
            <img src="Images/about_pic.jpg" alt="photo"/>
            <h2 style="color: #900; text-align: center; padding-top: 10px; margin-bottom: 0;">
                Khera Color Lab and Digital Studio
            </h2>
            <p >
              At Khera Color lab and Digital Studio, we transform dreams into reality,
              by printing them directly to any surface or canvas. Specialized in manufacturing
              of photo albums, designer apple albums, pillow printing, mug printing, digital 
              printing, calender making, and much more, we provide all high resolution photos
              printing solutions. Our entire range of printing services and unique collection of
              gallery products, make us the prior choice of our market. Right from small album 
              size photographs to large wall size prints and canvas, from photo albums to surface 
              printing, we expertise in all. All latest machines and our quality work speaks
              itself for what we are0.
              <br/>
              <br/>

              With over Years of experience, we are keen about the vision of an individual to create
              a lot more memories with us. Our photographers understand well about the needs and
              vision of customer to create those excellent moments and beading them into the thread of 
              happiness to make a record of ever-lasting moments at just a place which reminds their 
              mind onto those wonderful moments they made during their wedding or an event. With 
              pride we are pleased to share our client acclamation towards our work for its on-time
              delivery, quality, creativity and uniquity. 
            </p>
          </div><!-- about content div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>

@endsection