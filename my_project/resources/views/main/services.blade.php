@extends('main.layout')

@section('style')

    <!-- CSS Coding -->    
    <style>
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */
    
      #inner h1{
        text-align: center;
        margin-bottom: 50px;
        font-family: 'Roboto Slab', 'Times New Roman',serif;
      }
      
        
      .text{
        color: #000;
        font-size: 25px;
        clear: both;
        padding-top: 20px;
        opacity: 1;
        transition: 0.5s all ease-in-out;
        font-family: 'Raleway',Arial,sans-serif;
        font-weight: 400;
      }

      form{
        width: 100%;
        max-width: 900px;
        margin: 0 auto;
        margin-bottom: 30px;
        color: #000;
        float: right;
        text-align: right;
        margin-right: 31px;
      }

      input[type="text"]{
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        border: 2px solid #000;
        margin: 2px;
        padding:10px;
        width: 150px;
        margin-top: 20px;
      } 
      
      input[type="text"]:hover{
        background: #ccf;
        box-shadow: 2px 3px 5px #666; 
        border: 2px solid #ff9c00;
      }

      

      .main{
        float:right;
        width: 75%;
        padding: 10px;
        border-left: 1px solid #cfcfcf;
        background-color: transparent;
      }
      
      #secondary{
        float:left;
        width: 25%;
        padding-left: 10px;
        background: #transparent;
        height: 100%;
        margin: 0  ;
      }

      .search-btn{
        color: #300;
        background: #fff;
        border: 2px solid #300;
        padding: 9px;
        margin: 0px;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        font-weight: bold;
        margin-bottom: 20px;
        text-decoration: none;
      }
      .search-btn:hover{
        color: #fff;
        background: #000;
        border: 2px solid #000;    
      }

      div.img{
        margin-top: 12px;
        float: left;
        display: block;
        width: 20%;
        padding: 12px;
      }
      
      div.img img{
        display: block;
        transition: 0.5s all ease-in-out;
        transform: scale(1);
        transform-origin: 50% 50%; 
        opacity: 1;
        height: auto; 
        width: 100%;
        border-radius: 12px;
        box-shadow: 0px 7px 18px rgba(0, 0, 0, 0.80);
      }
      
      div.img img:hover{
        transform: scale(1.1);
        opacity: 1; 
      }

      .details{
        font-family: 'Roboto Slab','Times New Roman', serif;
        font-weight: 400;
        width: 60%;
        float:left;
        padding: 19px;
      }
      .more{
        float:left;
        width: 20%;
        margin-top: 100px;
      }

      .item{
        clear: both;
        margin-top: 0;
      }

      h2{
        margin-left: 36px;
      }

      .category{
        list-style-type: none;
        margin-top: 8px;
      }

      .category li{
        margin-top: 8px;
      }

      .category li a{
        color: #fff;
        text-decoration: none;
      }

    @media (max-width:767px) {
      .main{
        width:100%;
      }

      #secondary{
        width: 100%;
      }
      .search-btn{
        padding:2px;
      }
    }
      
    </style>

@endsection

@section('content')

 <section>
     <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <!-- Services heading -->
          <h1><span class="underline">{{$subtitle}}</span></h1>
          <div id="secondary">
            <form method="post" action="">
              <input type="text" name="search" id="search" placeholder="search"/>
            </form>
            <br />
            <h2>Categories</h2>
            <ul class="category">
              <li><strong>Deivery Method</strong>
                <!-- loop for displaying data -->
                <ul>
                  <li><a href="#">Album</a></li>
                  <li><a href="#">Pen drive</a></li>
                  <li><a href="#">Google Drive</a></li>
                  <li><a href="#">Hard Disk</a></li>
                  <li><a href="#">Prints</a></li>
                  <li><a href="#">CDs</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- Secondary div end -->
         
          <div class="main">
            <!-- loop for displaying data -->
            @foreach($services as $service)
              <div class="item">
              <div class="img">
                <img src="/images/{{ $service->image }}" alt="pic_s3"/>
              </div>
              <div class="details">
                <p><span style="font-weight: bold; color: #ff3">{{ $service->package_type }}</span>
                  : {{ $service->session_type }} session<br /><br />
                  <strong>Photo quality: </strong>{{ $service->photo_quality }}<br />
                  <strong>Photo size: </strong>{{ $service->photo_size }}<br />
                  <strong>Photo quantity: </strong>{{ $service->no_of_photos }}<br />
                  <strong>Delivery method: </strong>{{ $service->delivery_method }}<br />
                  <strong>Price: </strong>{{ $service->price }}</p>
                </div>
                <div class="more">
                  <a class="search-btn" 
                          href="/services/{{$service->package_type}}">
                    More info
                  </a>
                </div>
              </div>
            @endforeach
          </div><!-- main div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>

@endsection