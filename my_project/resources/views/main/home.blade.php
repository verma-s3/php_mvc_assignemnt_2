@extends('main.layout')

@section('style')

<style>
.left_width{
	margin-left: 50px;
}
</style>

@endsection


@section('content')

<section>
    <div id="container"><!-- container div id starting -->
        <div id="inner"><!-- inner div starting -->
            <h1 class="left_width"><span class="underline">Welcome to Khera Digital Studio</span></h1>
            @if (session('status'))
                <div class="alert alert-success left_width" role="alert">
                    {{ session('status') }}
                </div>
            @endif

           <p class="left_width"> You are logged in!</p>
        </div>
    </div>
</section>
@endsection

