@extends('main.layout')

@section('style')

<!-- CSS Coding -->    
    <style>
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */
    
      
      #contact_info img{
        margin-left: 50px;
        border: 2px solid #ff3;
      }
      
      
      #contact_info h2{
        margin-bottom: 0;
      }
      
      #contact_info h2:first-child{
        margin-top: 0;
      }
      
      #contact_info p{
        margin-top: 0;
      }
      
      .two_col{
        margin-left: 15px;
        float:left;
        column-count: 2;
      }
      
      table{
        width: 100%;
        max-width: 900px;
        margin: 0 auto;
        background: #fff;
        color: black;
      }
      
      caption{
        color: #300;
        background: #ccf;
        padding: 5px 0;
        font-size: 1.5em;
        font-family: 'Roboto Slab','Times New Roman',serif;
        font-weight: 700;
        border: 2px Solid #000;
      }
      
      table{
        border-spacing: 0;
        border-collapse: collapse;
        border: 2px Solid #000;
      }
      
      table th{
        background: #ffc;
        padding: 2px;
        border: 2px Solid #000;
      }
      
      td:nth-child(odd){
        background: #cff;
        border: 2px Solid #000;
        padding: 5px;
      }
      
      td:nth-child(even){
        background: #cfc;
        border: 2px Solid #000;
        padding: 5px;
      }
       /* Form CSS */
       
      form{
        width: 100%;
        max-width: 900px;
        margin: 0 auto;
        margin-bottom: 30px;
        color: #000;
      }
      
      fieldset{
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        border: 2px solid #000;
        background: #fff;
      }
      
      legend{
        background: #000;
        color: #fff;
        border-radius: 3px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -ms-border-radius: 3px;
        -o-border-radius: 3px;
        padding: 5px 10px;
      }
      
      /* Styling for labeling */
      
      label{
        padding: 10px 0 0 2px;
        width: 150px; 
        display: block;
        float: left;
      }
      
      /* addition of specific content in label fields */
      
      label.field:after {
        content: '*';
        color: #f00;
      }
      
      /* styling of specific input tags */
      
      input[type="text"],
      input[type="email"],
      input[list="province_name"],
      textarea{
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        border: 2px solid #000;
        margin: 2px;
        padding:10px;
        width: 250px;
      } 
      
      input[type="text"]:hover,
      input[type="email"]:hover,
      input[list="province_name"]:hover,
      textarea:hover{
        background: #ccf;
        box-shadow: 2px 3px 5px #666; 
        border: 2px solid #ff9c00;
      }
      
      input[type="submit"],
      input[type="reset"]{
        padding: 4px 6px;
        border: 2px solid #000;
        color: #fff  ;
        background-color: #000;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        -ms-border-radius: 5px;
        -o-border-radius: 5px;
        margin-left: 20px;
        font-family: 'Roboto Slab','Times New Roman',serif;
        font-weight: 400;
      } 
      
      input[type="submit"]:hover{
        background-color: #060;
        border-color: #060;
        color: #fff;
      } /* mouseover effect on submit button */
        
      input[type="reset"]:hover{
        background-color: #900;
        border-color: #900;
        color: #fff;
      } /* mouseover effect on reset button */
      
    </style>

@endsection

@section('content')

<div id="container"><!-- container div started -->
      <div id="inner"><!-- inner div started -->
        <div id="contact_info"><!-- content_info div starting -->
          <div>
            <h1><span class="underline">{{$subtitle}}</span></h1>
            <div class="two_col"><!-- two_col starting -->
              <h2>Address</h2>
              <address>
                Amritsar Road,<br />
                Near Khanna Hospital,<br />
                Moga,Punjab,India,<br />
                142001. 
              </address>
              <h2>Timings:</h2>
              <p>10.00 AM - 8.00 PM</p>
              <h2>Call on:</h2>
              <p>97812-40006, 97816-04346</p>
              <h2>E-mail:</h2>
              <p>kheracolorlabmoga@gmail.com</p>
            </div><!-- two_col ending -->
            <img src="Images/card.jpg" alt="card" />
          </div>
        </div><!-- content_info div ending -->
        <hr />
        <br />
        <div style="margin-top:30px;margin-bottom: 50px;"><!-- table div starting -->
          <table><!-- table starting -->
            <caption>Package information</caption>
            <tr >
              <th colspan="4" style="background: #fcf;">PACKAGE PRICES</th>
            </tr>
            <tr>
              <th>Package 1</th>
              <th>Package 2</th>
              <th>Package 3</th>
              <th>Package 4</th>
            </tr>
            <tr>
              <td>$150</td>
              <td>$250</td>
              <td>$350</td>
              <td>$450</td>
            </tr>
            <tr>
              <td>1 Person</td>
              <td>1-3 Person(s)</td>
              <td>1-2 Person(s)</td>
              <td>1-6 Person(s)</td>
            </tr>
            <tr>
              <td>15 Mintute Session</td>
              <td>20 Mintute Session</td>
              <td>30 Mintute Session</td>
              <td>45 Mintute Session</td>
            </tr>
            <tr>
              <td>10 Digital Images</td>
              <td>20 Digital Images</td>
              <td>30 Digital Images</td>
              <td>40 Digital Images</td>
            </tr>
            <tr>
              <td>Photo release</td>
              <td>Photo release + Facbook Cover</td>
              <td>Photo release</td>
              <td>Photo release + Facebook Cover</td>
            </tr>
          </table><!-- table ending -->
        </div><!-- table div ending -->
        <hr />
        <br />
        <div><!-- div for form started -->
          <h1 style="padding-left: 30px;">Contact Form:</h1>
          <p style="padding-left: 30px;">Please fill the form to chat with us:</p>
          <form id="first_form"
                name="first_form"
                method="post"
                action="http://www.scott-media.com/test/form_display.php"
                autocomplete="on" 
                > <!-- starting of request form -->
            <fieldset>
              <legend>Contact Information</legend>
              <p>All the (<span id="compulsory">*</span>) fields are mandatory</p>
              <p>
                <label for="First_name" class="field">First Name:</label> 
                  <input type="text" 
                         name="First_name" 
                         id="First_name" 
                         maxlength="40"  
                         placeholder="Type Your First name"
                         required="required" /><!-- First name field -->
                </p>

              <p>
                <label for="Last_name" class="field"> Last Name: </label> 
                  <input type="text" 
                         name="Last_name" 
                         id="Last_name" 
                         maxlength="40"
                         placeholder="Type Your Last name" 
                         required="required"/><!-- Last name field -->
              </p>
              
              <p>
              <label for="email_address" class="field">Email: </label>
                <input type="email" 
                       name="email_address" 
                       id="email_address" 
                       placeholder="e.g: soniaverma@gmail.com" 
                       required="required"/><!-- email input field -->
              </p>
              
              <p>
              <label for="comments">Comments: </label>
                <textarea id="comments"
                          name= "comments"
                          rows="4"
                          cols="50"
                          maxlength="50">
                </textarea><!-- text area field -->
              </p>
              
              <p>
                <label for="province">Select your Province:</label>
                  <input list="province_name"  id="province" name="province"/>
                  <datalist id="province_name">
                    <option value="Alberta">
                    <option value="British Coloumbia">
                    <option value="Manitoba">
                    <option value="Saskatchewan">
                    <option value="Toronto">
                    <option value="Quebec">
                  </datalist>
              </p>
              
              <p ><!-- action performed by submit and resrt button -->
                <input type="submit" value="Submit Application" /> &nbsp;  
                <input type="reset" value="Clear Application" /> &nbsp;
              </p>
              
            </fieldset>
          </form>
        </div><!-- div for form ending -->
      </div><!-- inner div ending -->
    </div><!-- container div ending -->

@endsection