@extends('main.layout')
@section('style')

    <!-- CSS Coding -->    
    <style>
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */

	  #inner h1{
	    text-align: center;
	    margin-bottom: 50px;
	    font-family: 'Roboto Slab', 'Times New Roman',serif;
	    font-weight: 700;
	    font-size: 1.8em; 
	  }

	  span{
	  	font-weight: bold;
	  	color: #ff3;
	  }

	  div.img{
	    margin-top: 8px;
	    margin-left: 10px;
	    float: left;
	    display: block;
	    width: 30%;
	    padding-left: 50px;
	  }

	  div.img img{
	    height: auto; 
	    width: 100%;
	    border-radius: 12px;
	    box-shadow: 0px 7px 18px rgba(0, 0, 0, 0.80);
	  }

	  .details{
	    font-family: 'Roboto Slab','Times New Roman', serif;
	    font-weight: 400;
	    width: 60%;
	    float:left;
	    padding-left: 130px;
	  }

	  .item{
	    clear: both;
	    margin-top: 0;
	  }

	  table,tr{
	    border-bottom: 2px solid #fff;
	    border-collapse: collapse;
	  }
	  td{
	    padding: 12px 50px;
	    border-bottom: 2px solid #fff;
	  }

	  .search-btn{
	    color: #300;
	    background: #fff;
	    border: 2px solid #300;
	    padding: 9px;
	    margin: 0px;
	    border-radius: 5px;
	    -webkit-border-radius: 5px;
	    -moz-border-radius: 5px;
	    -ms-border-radius: 5px;
	    -o-border-radius: 5px;
	    font-weight: bold;
	    text-decoration: none;
	    display: block;
	    margin : 25px auto;
	  }
	  .search-btn:hover{
	    color: #fff;
	    background: #000;
	    border: 2px solid #000;    
	  }
	  #cart{
	    font-family: 'Roboto Slab', 'Times New Roman',serif;
	    font-weight: 700;
	    margin: 0 auto;
	    display: block;
	    margin-right: 20px;
	    text-align: right;
	  }

	  #cart_info{
	    border: 2px solid #fff;
	    display: inline-block;
	    padding: 10px;
	  }
	  .back{
	    text-align: left;
	    color: #fff;
	    text-decoration: none;
	    margin-left: 50px;
	    text-decoration: underline;
	  }

	  .back::before{
	    content: '<- ';
	    color: #fff;
	  }

	  .add_cart{
	    color: #300;
	    margin-top: 6px;
	    text-decoration: none;
	    border: 2px solid #fff;
	    background: #fff;
	    border-radius: 8px;
	    padding: 3px 8px;
	  }
	  .add_cart:hover{
	    color: #fff;
	    background: #000;
	    border: 2px solid #000;
	  }
	  @media (max-width:767px) {
	    div.img{
	      display: block;
	      width: 100%;
	      padding: 0;
	    }

	    div.img img{
	      height: auto; 
	      width: 100%;
	    }

	    .details{
	      width: 100%;
	      padding-left: 50px;
	    }
	    .item{
	      clear: both;
	      margin-top: 0;
	    }
	    table,tr{
	      border-bottom: 2px solid #fff;
	      border-collapse: collapse;
	    }
	    td{
	      padding: 8px 20px;
	      border-bottom: 2px solid #fff;
	    }
	  }
	</style>
@endsection

@section('content')

<section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <a class="back" href="/services" title="service page">Back to Services</a>
          <!-- package detailed information -->
          <h1><span> {{ $service->package_type }}</span></h1>
          <div class="item">
              <div class="img">
                <img src="/Images/{{ $service->image }}" alt="pics"/>
              </div>
              <div class="details">
                <h2><span>Details</span></h2>
                <table>
                  <tr>
                    <td><span>Photo quality</span></td>
                    <td>{{ $service->photo_quality }}</td>
                  </tr>
                  <tr>
                    <td><span>Session time</span></td>
                    <td>{{ $service->session_time }}</td>
                  </tr>
                  <tr>
                    <td><span>Photo size</span></td>
                    <td>{{ $service->photo_size }}</td>
                  </tr>
                  <tr>
                    <td><span>Photo quantity</span></td>
                    <td>{{ $service->no_of_photos }}</td>
                  </tr>
                  <tr>
                    <td><span>Delivery method</span></td>
                    <td>{{ $service->delivery_method }}</td>
                  </tr>
                  <tr>
                    <td><span>Photographer Name</span></td>
                    <td>{{ $service->photographer_name }}</td>
                  </tr>
                  <tr>
                    <td><span>Available</span></td>
                    <td>{{ $service->availability }}</td>
                  </tr>
                  <tr>
                    <td><span>Price</span></td>
                    <td>${{ $service->price }}</td>
                  </tr>
                </table>
                <p><span>Description: </span>{{ $service->description }}</p>
              </div>             
            </div>
          
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
     
    </section>

@endsection