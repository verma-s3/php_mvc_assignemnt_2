@extends('main.layout')

@section('style')


    
    <!-- CSS Coding -->    
    <style>
      
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */
    
      #inner{
        margin-top: 0;
      }
      /* content div */
      div#welcome_content{
        background-color: #000;
        color: #fff;
        padding-left: 10px;
        padding-right: 10px;
        margin-bottom: 2px;
      }
        
      div#welcome_content h1{
        border-bottom: 1px solid #fff;
        padding-left: 8px;
        padding-top: 8px;
        margin-bottom: 0;
        font-family: 'Raleway','Arial', sans-serif;
        font-weight: 700;
      }
      
      div#welcome_content p{
        padding-bottom: 10px;
        font-family:'Roboto Slab','Times New Roman', serif;
        font-weight: 400;
      }
        
      /* information content div */
      
      div#info_content{
        background-color: rgba(255,255,255,0.3);
        color: #fff;
      
      }
      
      div#info_content h1{
        font-family: 'Raleway','Arial', sans-serif;
        font-weight:700;
        font-size: 1em;
        margin-bottom: 0;
        padding-top: 3px;
        text-transform: uppercase;
      }
      
      div#info_content p{
        font-family: 'Roboto Slab','Times New Roman', serif;
        font-weight: 400;
        font-size: 16px;
        margin-top: 0;
        margin-bottom: 0;
        padding-bottom: 2px;
        
      }
      
      div#info_content img{
        display: block;
        border: 2px solid #000;
        float: left;
        margin-right: 6px;
        transition: 0.4s all ease-in-out;
      }
      
      div#info_content img:hover{
        transform: scaley(1.1);
      }
      
      /* Gallery css */
      
      div#gallery{
        color: #fff;
        text-align: center;
        padding-top: 5px;
        font-size: 2em;
      }
      
      
      #pic{
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        width: 100%;
      }
      
      #pic img{
        flex-grow: 1;
      }
      
      #photo_link{
        text-align: center;
        margin: 25px auto 30px auto;
      }
      
      #photo_link a{
      background: #fff;
      color: #000;
      font-size: 1.2em;
      padding: 5px 25px;
      font-weight: bold;
      text-decoration: none;
      border-radius: 10px;
      border: 2px solid;
      }
      
      #photo_link a:hover{
        background: #900; 
        color: #fff;
        box-shadow: 0 0 0 1px #fff;
        font-weight: 400;
      }
      
      
      
      /* media query for index page only to make the gallery images more responsive */
      @media all and (max-width:614px){
        #pic{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
      }
      
      #pic img{
        flex-grow: 1;
      }
      
      img{
        max-width: 100%;
      }
      }
      
    </style>

@endsection

@section('content')


    <section>
      <div id="container"><!-- div container starting -->
        <div id="inner"><!-- div ending -->
          <div><img src="Images/main.jpg" alt="cover_image"  /></div>
          <div id="welcome_content"><!-- welcome_content div starting -->
            <h1>{{$subtitle}}</h1>
            <p>
              Welcome to Khera Color Lab and Digital Studio, moga's best photography and videography service
              dedicated to creating lasing memories for years to come. Our expertise team of photographers, 
              cinematographers, and artists do the work with great enthusiasm. Our proficient team of photographers
              always ready to take the creativity at par with their passion. We specialized in wedding shoots, 
              engagement shoots, pre-wedding and post-wedding shoots, martinity shoots, baby shoots, portfolios,
              and many more. Apart from this we provide the other services like apple album, wedding box and cards, 
              personalised gifts, etc. 
            </p>
          </div><!-- welcome_content div ending -->
          <div id="info_content"><!-- info_content div starting -->
            <img src="Images/front_page.jpg" alt="photo" />
            <h1>What We Do</h1>
            <p>
              We offer wedding phototgraphy, cinematography, ecommerce photography pre-wedding and 
              post-wedding shoots and many more.
            </p>

            <h1>What We Love</h1>
            <p>
              We embrace capturing real moments of love, affection and joy our artistic skills, play
              a vital role in having perfect pictures.
            </p>

            <h1>What We believe in</h1>
            <p>
              Photography is the combination of science and art and only experts can balance these two 
              vital spheres when it comes to capture moments. 
            </p>
          </div><!-- info_content div ending -->
          <div id="gallery"><!-- gallery div starting --> 
            <p>
              <span class="underline">Photo Gallery</span>
            </p>
          </div><!-- gallery div ending -->
          <div id="pic" ><!-- pic div starting -->
            <img src="Images/pic1.jpg" alt="pic1" />
            <img src="Images/pic2.jpg" alt="pic2" />
            <img src="Images/pic3.jpg" alt="pic3" />
            <img src="Images/pic4.jpg" alt="pic4" />
          </div><!-- pic div ending -->
          <div id="photo_link"><!-- photo_link div starting -->
            <a href="/photo">View more</a>
          </div><!-- photo_link div ending -->
        </div><!-- div inner ending -->
      </div><!-- div container ending -->
    </section>

@endsection
