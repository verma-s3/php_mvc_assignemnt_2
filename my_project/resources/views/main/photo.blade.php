@extends('main.layout')

@section('style')

    <style>
      /*
      00000000000000000000000000000000000000000000000000
      SSS      SSS       SSS    SSS  SSS  SSS        SSS
      OOO  OOOOOOO  OOO  OOO  O  OO  OOO  OOO  OOOO  OOO
      NNN      NNN  NNN  NNN  NN  N  NNN  NNN        NNN
      IIIIIII  III  III  III  III    III  III  IIII  III
      AAA      AAA       AAA  AAAA   AAA  AAA  AAAA  AAA
      00000000000000000000000000000000000000000000000000
      */
    
     
      #inner h1{
        text-align: center;
      }
      
      #inner p{
        margin-left: 10px;
        margin-right: 10px;
        margin-bottom: 60px;
        font-family: 'Raleway',Arial,sans-serif;
        font-weight: 400;
      }
      
      #photography{
        border-radius: 50px;
      }
      
      /* CSS for gallery effects */
      
      .gallery_pic{
        float: left;
        margin-bottom: 40px;
        margin-right: 60px;
        position: relative;
      }
      .gallery_pic img{
        border-radius: 40px 0 40px 0;
        border: 2px solid #fff;
        display: block;
        height: auto;
        width: 100%;
      }
      
      .gallery_pic:nth-child(odd){
        margin-left: 70px;
      }
      
      .gallery_pic:hover > .overlay{
        opacity: 1;
        cursor: pointer;
      }
      
      /* CSS Hover effects */
      
      .overlay{
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        height: 100%;
        width: 100%;
        background: rgba(153,0,0,80%);
        border-radius: 40px 0 40px 0;
        border: 2px solid #fff; 
        transition: .5s ease-in-out;
        opacity: 0;
      }
      
      .text{
        color: #fff;
        font-size: 20px;
        position: absolute;
        top: 45%;
        left: 50%;
        -webkit-transform: translate(-50%,-50%);
        -moz-transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        -o-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
        text-align: center;
      }
      
      
    </style>

@endsection
<!-- satrting section -->
@section('content')

<section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <h1><span class="underline">{{$subtitle}}</span></h1>
          <p>
            Khera Color Lab and Digital Studio has expertise team for the creative photography, do not metters
            what the event is. DSLR photos are quite popular nowadays. Blur background with focus on a person makes 
            photos more interesting and unique. Prople love to have these types of memorable momemts of their life.
            Our motive is not only to provide quality work to our customers but also to keep their expectations and
            requirements at first.
          </p>
          <div id="photograhy"><!-- photography starting -->
            <!-- gallery imagaes inside div starting -->
            <div class="gallery_pic">
              <img src="Images/g1.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Ankita and Lucky</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g2.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Purva and Rahul</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g3.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nisha and Vikas</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g4.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Pooja and Akash</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g5.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Aman and Deepak</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g6.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nidhi and Sandeep</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g7.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nitika and Rajan</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g8.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Raman and Sachin</p>
              </div>
            </div>
            <!-- gallery imagaes inside div ending -->
          </div><!-- photographer div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>

@endsection