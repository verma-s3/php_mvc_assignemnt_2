@extends('main.layout')

@section('style')
<style>
 /* Form CSS */
  .heading{
    font-size: 48px;
    color: #ff3;
    text-align: center;
    margin-bottom: 30px;
  }

  .form{
    margin-left: 100px;

  }
  
  /* Styling for labeling */
  
  label{
    padding: 10px 0 0 2px;
    width: 200px; 
    display: block;
    float: left;
    font-size: 18px;
    font-weight: bold;
  }
  
  /* addition of specific content in label fields */
  
  label.field:after {
    content: '*';
    color: #f00;
  }
  
  /* styling of specific input tags */
  
  input[type="text"],
  input[type="email"],
  input[type="password"],
  input[type="password_confirm"]
  textarea{
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
    border: 2px solid #000;
    margin: 2px;
    padding:10px;
    width: 250px;
  } 
  
  input[type="text"]:hover,
  input[type="email"]:hover,
  input[type="password"]:hover,
  input[type="password_confirm"]:hover,
  textarea:hover{
    background: #ccf;
    box-shadow: 2px 3px 5px #666; 
    border: 2px solid #ff9c00;
  }
  
  button,
  input[type="reset"]{
    padding: 4px 16px;
    border: 2px solid #000;
    color: #fff  ;
    background-color: #000;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    -o-border-radius: 5px;
    margin-top: 20px;
    font-family: 'Roboto Slab','Times New Roman',serif;
    font-weight: 400;
    font-size: 16px;
  } 
  button{
    margin-left: 180px;
  }
  input[type="reset"]{
    margin-left: 20px;
  }
  
  button:hover{
    background-color: #060;
    border-color: #060;
    color: #fff;
  } /* mouseover effect on submit button */
    
  input[type="reset"]:hover{
    background-color: #900;
    border-color: #900;
    color: #fff;
  } /* mouseover effect on reset button */
</style>
@endsection
@section('content')
    <section>
        <div id="container"><!-- container div started -->
            <div id="inner"><!-- inner div started -->
                <div class="col-md-8 form">
                    <div class="card">
                        <div class="card-header heading">Register Form</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
