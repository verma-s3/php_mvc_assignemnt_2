<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
        	'package_type'=>'Pre-wedding Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'48 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'500',
        	'delivery_method'=>'CD',
        	'price'=>'1299',
        	'image'=>'pre-wedding.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Post-wedding Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'12 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'400',
        	'delivery_method'=>'Pen Drive',
        	'price'=>'1099',
        	'image'=>'postwedding.jpg',
        	'photographer_name'=>'Sunny',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Wedding Photography',
        	'session_time'=>'5 hours',
        	'photo_quality'=>'24 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'450',
        	'delivery_method'=>'Pen Drive',
        	'price'=>'799',
        	'image'=>'wedding.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'New Born Photography',
        	'session_time'=>'3 hours',
        	'photo_quality'=>'32 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'200',
        	'delivery_method'=>'CD',
        	'price'=>'399',
        	'image'=>'new_born.jpg',
        	'photographer_name'=>'Evgheni',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'No'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Portfolio Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'48 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'100',
        	'delivery_method'=>'Album',
        	'price'=>'499',
        	'image'=>'protfolio.jpg',
        	'photographer_name'=>'Sunny',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'No'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Family Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'12 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'160',
        	'delivery_method'=>'Hard disk',
        	'price'=>'359',
        	'image'=>'family.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Birthday Photography',
        	'session_time'=>'4 hours',
        	'photo_quality'=>'24 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'200',
        	'delivery_method'=>'Pen Drive',
        	'price'=>'449',
        	'image'=>'birthday.jpg',
        	'photographer_name'=>'Ibaljeet',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Baby Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'32 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'360',
        	'delivery_method'=>'CD',
        	'price'=>'1099',
        	'image'=>'baby.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Event Photography',
        	'session_time'=>'6 hours',
        	'photo_quality'=>'48 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'150',
        	'delivery_method'=>'Hard disk',
        	'price'=>'599',
        	'image'=>'event.jpg',
        	'photographer_name'=>'Sunny',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Modeling Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'12 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'160',
        	'delivery_method'=>'Album',
        	'price'=>'699',
        	'image'=>'modeling.jpg',
        	'photographer_name'=>'Evgheni',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Night Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'32 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'180',
        	'delivery_method'=>'Pen Drive',
        	'price'=>'299',
        	'image'=>'night.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Rain Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'48 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'400',
        	'delivery_method'=>'Album',
        	'price'=>'799',
        	'image'=>'rain.jpg',
        	'photographer_name'=>'Evgheni',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Real Estate Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'24 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'120',
        	'delivery_method'=>'CD',
        	'price'=>'299',
        	'image'=>'real_estate.jpg',
        	'photographer_name'=>'Ibaljeet',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'No'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Sports Photography',
        	'session_time'=>'7 hours',
        	'photo_quality'=>'12 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'30',
        	'delivery_method'=>'Hard disk',
        	'price'=>'399',
        	'image'=>'sports.jpg',
        	'photographer_name'=>'Sunny',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Fashion  Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'48 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'10',
        	'delivery_method'=>'Album',
        	'price'=>'199',
        	'image'=>'fashion.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Sunrise/Sunset Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'32 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'45',
        	'delivery_method'=>'Hard Disk',
        	'price'=>'1099',
        	'image'=>'sun.jpg',
        	'photographer_name'=>'Ibaljeet',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Time Lapse Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'24 MP',
        	'photo_size'=>'16*20',
        	'no_of_photos'=>'50',
        	'delivery_method'=>'CD',
        	'price'=>'1299',
        	'image'=>'time.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Underwater Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'12 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'30',
        	'delivery_method'=>'Hard disk',
        	'price'=>'1599',
        	'image'=>'underwater.jpg',
        	'photographer_name'=>'Sunny',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Wildlife Photography',
        	'session_time'=>'2 hours',
        	'photo_quality'=>'32 MP',
        	'photo_size'=>'11*16',
        	'no_of_photos'=>'450',
        	'delivery_method'=>'Pen Drive',
        	'price'=>'459',
        	'image'=>'portrait.jpg',
        	'photographer_name'=>'Ibaljeet',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
        DB::table('services')->insert([
        	'package_type'=>'Still life Photography',
        	'session_time'=>'1 hours',
        	'photo_quality'=>'24 MP',
        	'photo_size'=>'12*20',
        	'no_of_photos'=>'15',
        	'delivery_method'=>'Album',
        	'price'=>'269',
        	'image'=>'stilllife.jpg',
        	'photographer_name'=>'Sonia Verma',
        	'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        	'availability'=>'Yes'
        ]);
    }
}
